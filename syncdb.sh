#!/bin/bash
REMOTE_HOST="user@www.domain.com"
REMOTE_MYSQL_HOST="localhost"
REMOTE_MYSQL_DB="remote_db_name"
REMOTE_MYSQL_USER="remote_db_user"
REMOTE_MYSQL_PASS="remote_db_pass"
REMOTE_BASE_URL="remote.host.com"
LOCAL_MYSQL_HOST="localhost"
LOCAL_MYSQL_DB="local_db_name"
LOCAL_MYSQL_USER="local_db_user"
LOCAL_MYSQL_PASS="local_db_pass"
LOCAL_BASE_URL="www.domain.local"

if [[ `ssh $REMOTE_HOST 'test -e ~/'$REMOTE_MYSQL_DB'.tmp.sql && echo exists'` == *exists* ]]; then
  echo "Backup is currently being executed by another process. Please try again in a few moments."
  exit 1
fi

echo "Creating backup of remote database"
ssh $REMOTE_HOST 'mysqldump -h '$REMOTE_MYSQL_HOST' -u '$REMOTE_MYSQL_USER' -p'$REMOTE_MYSQL_PASS' '$REMOTE_MYSQL_DB' > ~/'$REMOTE_MYSQL_DB'.tmp.sql' &> /dev/null
ssh $REMOTE_HOST 'tar -czf '$REMOTE_MYSQL_DB'.tmp.sql.tar.gz '$REMOTE_MYSQL_DB'.tmp.sql' &> /dev/null

echo "Transferring backup from remote to local"
scp $REMOTE_HOST:~/$REMOTE_MYSQL_DB.tmp.sql.tar.gz ~/
ssh $REMOTE_HOST 'rm ~/'$REMOTE_MYSQL_DB'.tmp*'

echo "Extracting backup"
tar -xzf ~/$REMOTE_MYSQL_DB.tmp.sql.tar.gz -C ~/
echo "Updating config"
sed "s/$REMOTE_BASE_URL/$LOCAL_BASE_URL/g" ~/$REMOTE_MYSQL_DB.tmp.sql > ~/$REMOTE_MYSQL_DB.tmp.new.sql
echo "Reloading local database (may take few moments)"
mysql -u $LOCAL_MYSQL_USER -h $LOCAL_MYSQL_HOST -p$LOCAL_MYSQL_PASS $LOCAL_MYSQL_DB < ~/$REMOTE_MYSQL_DB.tmp.new.sql &> /dev/null

# Clean local temp files
rm ~/$REMOTE_MYSQL_DB.tmp*

echo "Complete!"
